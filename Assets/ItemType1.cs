using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public enum ItemType
    {
        COIN,
        BIGCOIN,
        POWERUP,
        POWERDOWN,
    }

    // Start is called before the first frame update